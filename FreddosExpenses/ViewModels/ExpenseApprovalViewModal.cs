﻿using FreddosExpenses.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FreddosExpenses.ViewModels
{
    public class ExpenseApprovalViewModal
    {
        public int Id { get; set; }
        public string ApprovalStatus { get; set; }
        [Display(Name = "Submitted Date")]
        public DateTime SubmittedDate { get; set; }
        [Display(Name = "Expense Date")]
        public DateTime ExpenseDate { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        [Display(Name = "Created By")]
        public string CreatedByUserName { get; set; }
        public virtual ICollection<ExpenseReceipt> ExpenseReceipts { get; set; }
    }
}
