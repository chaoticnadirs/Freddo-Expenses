﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreddosExpenses.ViewModels
{
    public class ExpenseApprovalProcessingSelectionViewModal
    {
        public List<ExpenseApprovalProcessingViewModal> ExpenseApproval { get; set; }
        public ExpenseApprovalProcessingSelectionViewModal()
        {
            this.ExpenseApproval = new List<ExpenseApprovalProcessingViewModal>();
        }
    }
}
