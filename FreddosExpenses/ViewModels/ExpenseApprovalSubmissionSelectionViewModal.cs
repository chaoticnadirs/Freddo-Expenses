﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreddosExpenses.ViewModels
{
    public class ExpenseApprovalSubmissionSelectionViewModal
    {
        public List<ExpenseApprovalSubmissionViewModal> ExpenseApproval { get; set; }
        public ExpenseApprovalSubmissionSelectionViewModal()
        {
            this.ExpenseApproval = new List<ExpenseApprovalSubmissionViewModal>();
        }
    }
}
