﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreddosExpenses.ViewModels
{
    public class ExpenseApprovalSelectionViewModal
    {
        public List<ExpenseApprovalViewModal> ExpenseApproval { get; set; }
        public ExpenseApprovalSelectionViewModal()
        {
            this.ExpenseApproval = new List<ExpenseApprovalViewModal>();
        }
    }
}
