﻿using System;
namespace FreddosExpenses.ViewModels
{
    public class HomeViewModel
    {
        public bool ShowExpenseLoging
        {
            get;
            set;
        }

		public bool ShowExpenseSubmission
		{
			get;
			set;
		}

		public bool ShowExpenseApproval
		{
			get;
			set;
		}

		public bool ShowExpenseRevision
		{
			get;
			set;
		}
    }
}
