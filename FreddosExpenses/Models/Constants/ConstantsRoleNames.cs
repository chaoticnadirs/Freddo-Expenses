﻿namespace FreddosExpenses.Models.Constants
{
    public class ConstantsRoleNames
    {
        public const string Finance = "Finance";
        public const string Employee = "Employee";
        public const string Manager = "Manager";
    }
}