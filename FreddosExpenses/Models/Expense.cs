﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreddosExpenses.Models
{
    public class Expense
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Display(Name="Entry Date")]
        public DateTime SubmittedDate { get; set; }
        [Display(Name = "Expense Date")]
        public DateTime ExpenseDate { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        [Display(Name = "Created By")]
        public string CreatedByUserName { get; set; }
        public string CreatedByUserId { get; set; }
        public ICollection<IFormFile> Images { get; set; }
        public virtual ICollection<ExpenseReceipt> ExpenseReceipts { get; set; }
        public virtual ICollection<ExpenseApproval> ExpenseApprovals { get; set; }
    }

    public class ExpenseReceipt
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ExpenseId { get; set; }
        public string UserUploadedFileName { get; set; }
        public string StoredFileName { get; set; }
        public string ContentType { get; set; }
        public virtual Expense Expense { get; set; }
    }

    public class ExpenseApproval
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ExpenseId { get; set; }
        public bool IsSubmitted { get; set; }
        public bool IsApproved { get; set; }
        public bool IsRejected { get; set; }
        public bool IsProcessed { get; set; }
        public string Comment { get; set; }
        public virtual Expense Expense { get; set; }
    }
}
