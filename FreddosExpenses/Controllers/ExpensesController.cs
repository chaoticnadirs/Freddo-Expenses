﻿using FreddosExpenses.Data;
using FreddosExpenses.Models;
using FreddosExpenses.Models.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace FreddosExpenses.Controllers
{
    [Authorize(Roles = ConstantsRoleNames.Employee + "," + ConstantsRoleNames.Manager)]
	public class ExpensesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IOptions<ApplicationConfiguration> _optionsApplicationConfiguration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly string _expenseReceiptPath;
        private readonly UserManager<ApplicationUser> _userManager;
        private  string _userId;
        private  string _userName;

        public ExpensesController(
            ApplicationDbContext context, 
            IOptions<ApplicationConfiguration> o, 
            IHostingEnvironment h,
            UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
            _optionsApplicationConfiguration = o;
            _hostingEnvironment = h;
            _expenseReceiptPath = string.Format("{0}/{1}/", h.WebRootPath, "receipts");
            //_userId = _userManager.GetUserId(User);
            //_userName = _userManager.GetUserName(User);
        }

        // GET: Expenses
        public async Task<IActionResult> Index()
        {
            _userId = _userManager.GetUserId(User);
            return View(await _context.Expense
                .Include(r => r.ExpenseApprovals)
                .Where(item => item.CreatedByUserId == _userId && (item.ExpenseApprovals.Count == 0 || item.ExpenseApprovals.OrderByDescending(p => p.Id).Select(status => status.IsSubmitted == false && status.IsApproved == false && status.IsProcessed == false).FirstOrDefault()))
                .ToListAsync());
        }

        // GET: Expenses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expense = await _context.Expense
                .Include(r => r.ExpenseReceipts)
                .Include(r => r.ExpenseApprovals)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (expense == null)
            {
                return NotFound();
            }

            return View(expense);
        }

        // GET: Expenses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Expenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Expense expense)
        {
            if (ModelState.IsValid)
            {
                expense.SubmittedDate = DateTime.Now;
                expense.ExpenseReceipts = new List<ExpenseReceipt>();
                expense.ExpenseApprovals = new List<ExpenseApproval>();

                _userId = _userManager.GetUserId(User);
                _userName = _userManager.GetUserName(User);

                expense.CreatedByUserName = _userName;
                expense.CreatedByUserId = _userId;

                

                expense.ExpenseApprovals.Add(new ExpenseApproval()
                {
                    ExpenseId = expense.Id,
                    Comment = string.Empty,
                    IsSubmitted = false
                });

                if (expense.Images != null && expense.Images.Count > 0)
                {
                    foreach (var file in expense.Images)
                    {
                        if (file.Length > 0)
                        {
                            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                            var storedfilename = string.Format("{0}_{1}", Guid.NewGuid().ToString("N"), fileName);
                            // Extension method update RC2 has removed this 
                            await file.SaveAsAsync(Path.Combine(_expenseReceiptPath, storedfilename));

                            if (expense.ExpenseReceipts.Count > 0)
                            {
                                expense.ExpenseReceipts.ToList()[0].ContentType = file.ContentType;
                                expense.ExpenseReceipts.ToList()[0].StoredFileName = storedfilename;
                                expense.ExpenseReceipts.ToList()[0].UserUploadedFileName = fileName;
                            }
                            else
                            {
                                expense.ExpenseReceipts.Add(new ExpenseReceipt()
                                {
                                    ContentType = file.ContentType,
                                    StoredFileName = storedfilename,
                                    UserUploadedFileName = fileName
                                });
                            }
                        }
                    }
                }

                _context.Add(expense);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(expense);
        }

        // GET: Expenses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expense = await _context.Expense
                .Include(r => r.ExpenseReceipts)
                .Include(r => r.ExpenseApprovals)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (expense == null)
            {
                return NotFound();
            }
            return View(expense);
        }

        // POST: Expenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Expense expense)
        {
            if (id != expense.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var existingExpense = _context.Expense
                            .Include(r => r.ExpenseReceipts)
                            .Include(r => r.ExpenseApprovals)
                            .SingleOrDefault(m => m.Id == id);

                    if (existingExpense != null)
                    {
                        _context.Entry(existingExpense).CurrentValues.SetValues(expense);
                    }

                    existingExpense.SubmittedDate = DateTime.Now;

                    _userId = _userManager.GetUserId(User);
                    _userName = _userManager.GetUserName(User);

                    existingExpense.CreatedByUserName = _userName;
                    existingExpense.CreatedByUserId = _userId;

                    existingExpense.ExpenseApprovals.Add(new ExpenseApproval()
                    {
                        ExpenseId = existingExpense.Id,
                        Comment = string.Empty,
                        IsSubmitted = false
                    });

                    if (expense.Images != null && expense.Images.Count > 0)
                    {
                        if (existingExpense.ExpenseReceipts == null)
                        {
                            existingExpense.ExpenseReceipts = new List<ExpenseReceipt>();
                        }

                        foreach (var file in expense.Images)
                        {
                            if (file.Length > 0)
                            {
                                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                                var storedfilename = string.Format("{0}_{1}", Guid.NewGuid().ToString("N"), fileName);
                                // Extension method update RC2 has removed this 
                                await file.SaveAsAsync(Path.Combine(_expenseReceiptPath, storedfilename));

                                if (existingExpense.ExpenseReceipts.Count > 0)
                                {
                                    existingExpense.ExpenseReceipts.ToList()[0].ExpenseId = existingExpense.Id;
                                    existingExpense.ExpenseReceipts.ToList()[0].ContentType = file.ContentType;
                                    existingExpense.ExpenseReceipts.ToList()[0].StoredFileName = storedfilename;
                                    existingExpense.ExpenseReceipts.ToList()[0].UserUploadedFileName = fileName;
                                }
                                else
                                {
                                    existingExpense.ExpenseReceipts.Add(new ExpenseReceipt()
                                    {
                                        ContentType = file.ContentType,
                                        StoredFileName = storedfilename,
                                        UserUploadedFileName = fileName
                                    });
                                }
                                
                            }
                        }
                    }

                    _context.Update(existingExpense);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpenseExists(expense.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(expense);
        }

        // GET: Expenses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expense = await _context.Expense
                .Include(r => r.ExpenseReceipts)
                .Include(r => r.ExpenseApprovals)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (expense == null)
            {
                return NotFound();
            }

            return View(expense);
        }

        // POST: Expenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var expense = await _context.Expense.SingleOrDefaultAsync(m => m.Id == id);
            _context.Expense.Remove(expense);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpenseExists(int id)
        {
            return _context.Expense.Any(e => e.Id == id);
        }
    }
}
