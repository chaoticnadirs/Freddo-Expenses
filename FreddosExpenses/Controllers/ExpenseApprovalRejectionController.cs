﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FreddosExpenses.Data;
using FreddosExpenses.ViewModels;
using FreddosExpenses.Models;

namespace FreddosExpenses.Controllers
{
    public class ExpenseApprovalRejectionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExpenseApprovalRejectionController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ExpenseApprovalRejection
        public async Task<IActionResult> Index()
        {
            var result = await _context.Expense
                 .Where(item => item.ExpenseApprovals.OrderByDescending(p => p.Id).Select(status => status.IsSubmitted == true).FirstOrDefault())
                 .ToListAsync();
            var model = new ExpenseApprovalSelectionViewModal();


            foreach (var expense in result)
            {
                var editorViewModel = new ExpenseApprovalViewModal()
                {
                    Amount = expense.Amount,
                    ExpenseDate = expense.ExpenseDate,
                    SubmittedDate = expense.SubmittedDate,
                    Description = expense.Description,
                    Id = expense.Id,
                    CreatedByUserName = expense.CreatedByUserName
                };

                model.ExpenseApproval.Add(editorViewModel);
            }

            return View(model);
        }

        // GET: ExpenseApprovalRejection/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expense = await _context.Expense
                .Include(r => r.ExpenseReceipts)
                .SingleOrDefaultAsync(m => m.Id == id);

            var editorViewModel = new ExpenseApprovalViewModal();

            if (Expense == null)
            {
                return NotFound();
            }

            editorViewModel = new ExpenseApprovalViewModal()
            {
                Amount = Expense.Amount,
                ExpenseDate = Expense.ExpenseDate,
                SubmittedDate = Expense.SubmittedDate,
                Description = Expense.Description,
                Id = Expense.Id,
                ExpenseReceipts = Expense.ExpenseReceipts,
                CreatedByUserName = Expense.CreatedByUserName
            };

            return View(editorViewModel);
        }

        // GET: ExpenseApprovalRejection/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ExpenseApprovalRejection/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpenseApprovalSelectionViewModal Expense)
        {
            if (ModelState.IsValid)
            {
                foreach (var expenseApproval in Expense.ExpenseApproval)
                {
                    var existingExpense = await _context.Expense
                        .Include(r => r.ExpenseReceipts)
                        .Include(r => r.ExpenseApprovals)
                        .SingleOrDefaultAsync(m => m.Id == expenseApproval.Id);

                    if (existingExpense != null)
                    {
                        if (expenseApproval.ApprovalStatus != null)
                        {
                            existingExpense.ExpenseApprovals.Add(new ExpenseApproval()
                            {
                                ExpenseId = existingExpense.Id,
                                Comment = expenseApproval.Comment,
                                IsSubmitted = false,
                                IsRejected = expenseApproval.ApprovalStatus.ToUpper() == "REJECTED",
                                IsApproved = expenseApproval.ApprovalStatus.ToUpper() == "APPROVED"
                            });

                            _context.Entry(existingExpense).CurrentValues.SetValues(existingExpense);
                            _context.Update(existingExpense);
                            await _context.SaveChangesAsync();
                        }
                    }
                }
            }
            return RedirectToAction("Index"); 
        }

        // GET: ExpenseApprovalRejection/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expense = await _context.Expense.SingleOrDefaultAsync(m => m.Id == id);
            if (Expense == null)
            {
                return NotFound();
            }
            return View(Expense);
        }

        // POST: ExpenseApprovalRejection/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Approved,Rejected,Id,SubmittedDate,ExpenseDate,Amount,Description")] Expense Expense)
        {
            if (id != Expense.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(Expense);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpenseExists(Expense.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(Expense);
        }

        // GET: ExpenseApprovalRejection/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expense = await _context.Expense
                .SingleOrDefaultAsync(m => m.Id == id);
            if (Expense == null)
            {
                return NotFound();
            }

            return View(Expense);
        }

        // POST: ExpenseApprovalRejection/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Expense = await _context.Expense.SingleOrDefaultAsync(m => m.Id == id);
            _context.Expense.Remove(Expense);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpenseExists(int id)
        {
            return _context.Expense.Any(e => e.Id == id);
        }
    }
}
