﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FreddosExpenses.Models;
using Microsoft.AspNetCore.Identity;
using FreddosExpenses.Models.Constants;
using FreddosExpenses.ViewModels;

namespace FreddosExpenses.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private ApplicationUser _user;

        public HomeController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
			if (_signInManager.IsSignedIn(User))
			{
                _user = _userManager.GetUserAsync(User).Result;
			}
            var model = new HomeViewModel();

            var isManager = IsUserInRole(ConstantsRoleNames.Manager);

            model.ShowExpenseLoging = IsUserInRole(ConstantsRoleNames.Employee) || isManager;

            model.ShowExpenseSubmission = IsUserInRole(ConstantsRoleNames.Employee) || isManager;

            model.ShowExpenseApproval = isManager;

            model.ShowExpenseRevision = IsUserInRole(ConstantsRoleNames.Finance) || isManager;

            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public bool IsUserInRole(string roleName)
        {
            if(_user != null)
            {
                var res = _userManager.IsInRoleAsync(_user, roleName).Result;
				return res;    
            }

            return false;
		}
    }
}
