﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FreddosExpenses.Data;
using FreddosExpenses.ViewModels;
using FreddosExpenses.Models;

namespace FreddosExpenses.Controllers
{
    public class ExpenseApprovalProcessingController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExpenseApprovalProcessingController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ExpenseApprovalProcessing
        public async Task<IActionResult> Index()
        {
            var result = await _context.Expense
                .Where(item => item.ExpenseApprovals.OrderByDescending(p => p.Id).Select(status => status.IsSubmitted == false && status.IsApproved == true && status.IsProcessed == false).FirstOrDefault())
                .ToListAsync();
            var model = new ExpenseApprovalProcessingSelectionViewModal();


            foreach (var expense in result)
            {
                var editorViewModel = new ExpenseApprovalProcessingViewModal()
                {
                    Amount = expense.Amount,
                    ExpenseDate = expense.ExpenseDate,
                    Selected = false,
                    SubmittedDate = expense.SubmittedDate,
                    Description = expense.Description,
                    Id = expense.Id,
                    CreatedByUserName = expense.CreatedByUserName
                };

                model.ExpenseApproval.Add(editorViewModel);
            }

            return View(model);
        }

        // GET: ExpenseApprovalProcessing/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expense = await _context.Expense
                .Include(r => r.ExpenseReceipts)
                .SingleOrDefaultAsync(m => m.Id == id);

            var editorViewModel = new ExpenseApprovalProcessingViewModal();

            if (Expense == null)
            {
                return NotFound();
            }

            editorViewModel = new ExpenseApprovalProcessingViewModal()
            {
                Amount = Expense.Amount,
                ExpenseDate = Expense.ExpenseDate,
                Selected = false,
                SubmittedDate = Expense.SubmittedDate,
                Description = Expense.Description,
                Id = Expense.Id,
                ExpenseReceipts = Expense.ExpenseReceipts,
                CreatedByUserName = Expense.CreatedByUserName
            };

            return View(editorViewModel);
        }

        // GET: ExpenseApprovalProcessing/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ExpenseApprovalProcessing/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpenseApprovalProcessingSelectionViewModal ExpensesForApprovalList)
        {
            if (ModelState.IsValid)
            {
                var result = ExpensesForApprovalList.ExpenseApproval.OfType<ExpenseApprovalProcessingViewModal>().Where(s => s.Selected == true);

                foreach (var expenseApproval in result)
                {
                    var existingExpense = await _context.Expense
                        .Include(r => r.ExpenseReceipts)
                        .Include(r => r.ExpenseApprovals)
                        .SingleOrDefaultAsync(m => m.Id == expenseApproval.Id);

                    if (existingExpense != null)
                    {
                        existingExpense.ExpenseApprovals.Add(new ExpenseApproval()
                        {
                            ExpenseId = existingExpense.Id,
                            Comment = string.Empty,
                            IsProcessed = true
                        });

                        _context.Entry(existingExpense).CurrentValues.SetValues(existingExpense);
                        _context.Update(existingExpense);
                        await _context.SaveChangesAsync();
                    }
                }
            }
            return RedirectToAction("Index");
        }

        // GET: ExpenseApprovalProcessing/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expense = await _context.Expense.SingleOrDefaultAsync(m => m.Id == id);
            if (Expense == null)
            {
                return NotFound();
            }
            return View(Expense);
        }

        // POST: ExpenseApprovalProcessing/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Selected,Id,SubmittedDate,ExpenseDate,Amount,Description")] Expense Expense)
        {
            if (id != Expense.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(Expense);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpenseExists(Expense.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(Expense);
        }

        // GET: ExpenseApprovalProcessing/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expense = await _context.Expense
                .SingleOrDefaultAsync(m => m.Id == id);
            if (Expense == null)
            {
                return NotFound();
            }

            return View(Expense);
        }

        // POST: ExpenseApprovalProcessing/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Expense = await _context.Expense.SingleOrDefaultAsync(m => m.Id == id);
            _context.Expense.Remove(Expense);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpenseExists(int id)
        {
            return _context.Expense.Any(e => e.Id == id);
        }
    }
}
