﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FreddosExpenses.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExpenseReceipt_ExpenseApprovalViewModal_ExpenseApprovalViewModalId",
                table: "ExpenseReceipt");

            migrationBuilder.DropTable(
                name: "ExpenseApprovalViewModal");

            migrationBuilder.DropIndex(
                name: "IX_ExpenseReceipt_ExpenseApprovalViewModalId",
                table: "ExpenseReceipt");

            migrationBuilder.DropColumn(
                name: "ExpenseApprovalViewModalId",
                table: "ExpenseReceipt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExpenseApprovalViewModalId",
                table: "ExpenseReceipt",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ExpenseApprovalViewModal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    Approved = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ExpenseDate = table.Column<DateTime>(nullable: false),
                    Rejected = table.Column<bool>(nullable: false),
                    SubmittedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpenseApprovalViewModal", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExpenseReceipt_ExpenseApprovalViewModalId",
                table: "ExpenseReceipt",
                column: "ExpenseApprovalViewModalId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExpenseReceipt_ExpenseApprovalViewModal_ExpenseApprovalViewModalId",
                table: "ExpenseReceipt",
                column: "ExpenseApprovalViewModalId",
                principalTable: "ExpenseApprovalViewModal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
