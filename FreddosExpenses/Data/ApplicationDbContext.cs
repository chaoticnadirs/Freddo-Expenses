﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FreddosExpenses.Models;
using FreddosExpenses.ViewModels;

namespace FreddosExpenses.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<FreddosExpenses.Models.Expense>().Ignore(t => t.Images);
            builder.Entity<FreddosExpenses.Models.Expense>().HasKey(x => x.Id);
            builder.Entity<FreddosExpenses.Models.ExpenseReceipt>().HasKey(x => x.Id);
            builder.Entity<FreddosExpenses.Models.ExpenseApproval>().HasKey(x => x.Id);

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<FreddosExpenses.Models.Expense> Expense { get; set; }
    }
}
